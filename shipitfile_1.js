const util = require('util')
// npx shipit --shipitfile shipitfile_1.js default
module.exports = shipit => {
  shipit.initConfig({
    default: {
      servers: [
        'mrussitto@10.254.244.112'
      ],
      verboseSSHLevel: 0
    },
  })

  shipit.task('copyLogs', async () => {
    await shipit.copyFromRemote(
      '/home/mrussitto/repos/api_gateway/logs/ag.err-31.log',
      '/home/flow/Development',
    )
  })

  shipit.task('default', () => {
    shipit.start('copyLogs')
  })
}
