const deploy = require('shipit-deploy')
const util = require('util')

module.exports = shipit => {
  deploy(shipit)

  const appName = 'shipit-demo'
  const deployTo = `/home/mrussitto/repos/${appName}`
  const workspace = `/tmp/${appName}`

  shipit.initConfig({
    default: {
      keepReleases: 5,
      deleteOnRollback: true,
      shallowClone: true,
      workspace,
      deployTo,
      ignores: [
        '.git',
        'node_modules'
      ],
      repositoryUrl: 'git@bitbucket.org:teco_rhynl/shipit-demo.git',
      servers: [
        'user@server1',
        'user@server2',
        'user@server3'
      ],
      verboseSSHLevel: 0
    },
    development: {
      branch: 'master'
    },
    master: {
      branch: 'master'
    }
  })

  shipit.blTask('npm:install', async () => {
    const cmd = util.format('cd %s && npm i', shipit.releasePath)

    return shipit.remote(cmd)
  })

  shipit.blTask('pm2:startOrRestart', async () => {
    const current = `${shipit.config.deployTo}/current`
    const schema = shipit.config.pm2 && shipit.config.pm2.script ? shipit.config.pm2.script : 'ecosystem.config.js'

    const cmd = util.format('cd $(realpath %s) && pm2 reload --env %s %s',
      current, shipit.environment, schema)

    return shipit.remote(cmd)
  })

  shipit.task('postpublished', ['npm:install'])
  shipit.task('postdeployed', ['pm2:startOrRestart'])
  shipit.task('postrollbacked', ['pm2:startOrRestart'])

  shipit.on('published', async () => shipit.start('postpublished'))

  shipit.on('deployed', async () => shipit.start('postdeployed'))

  shipit.on('rollbacked', async () => shipit.start('postrollbacked'))
}
