# SHIPIT

> Universal automation and deployment tool <img src="https://assets-cdn.github.com/images/icons/emoji/unicode/26f5.png" alt="boat" height="15">

---

## Prerequisitos

* nodejs y npm instalados
* ejecutar `npm i`
* Para ver la presentación `npm run presentation`

---

## Agenda

1. Introducción

2. Shipit-deploy

3. Deploy con Shipit

4. Usando Shipit

---

## Introducción

Shipit es una herramienta de automatizacón remota que soporta la codificación y ejecución de cualquier tipo de tareas
y gracias al plugin a su plugin oficial, permite realizar tareas especificas de deploy, si bien no se limita solo a esto.

### caracteristicas

* Permite realizar deploys de manera confiable en multiples servidores de manera simultanea en paralele o en cascada.

* Ejecucion de tareas arbitraias vía ssh.

* Tiene un flujo de tareas que permite ejecutar tareas y dependencias con maxima concurrencia.

* Permite extender sus capacidades de manera sencilla.

* Esta escrito en JavaScript.

---

## Recetas

### Traer logs desde el servidor remoto

```js
const util = require('util')

module.exports = shipit => {
  shipit.initConfig({
    default: {
      servers: [
        'user@server'
      ],
      verboseSSHLevel: 0
    },
  })

  shipit.task('copyLogs', async () => {
    await shipit.copyFromRemote(
      '/var/logs/ag.err-31.log',    "http-server": "^0.11.1",
      '/home/user/logs',
    )
  })

  shipit.task('default', () => {
    shipit.start('copyLogs')
  })
}
```

---

## Shipit-deploy

Es el plugin oficial que extiende Shipit para facilitar las tareas de deploy

### Caracteristicas

* Permite hacer deploy por branch, tag o commit

* Se puede agregar nuevos comportamientos usando hooks

* Permite hacer el build local o remotamente

* Hacer rollback es muy sencillo

---

## Deploy con Shipit

* Crear un archivo con el nombre `shipitfile.js`

```js
module.exports = shipit => {
  require('shipit-deploy')(shipit)

  shipit.initConfig({
    default: {
      deployTo: '/var/apps/super-project',
      repositoryUrl: 'https://github.com/user/super-project.git',
    },
    dev: {
      servers: 'deploy@develop.super-project.com',
    },
  })
}
```

* Para hacer un deploy Ejecutar el comando:

```shell
# corre un deploy con la configuración dev
npx shipit dev deploy
```

* Y si algo sale mal, hacer un rollback es así de fácil

```shell
npx shipit dev rollback
```
