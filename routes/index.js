var express = require('express');
var router = express.Router();

router.get('/:name', function(req, res) {
  const name = req.params.name
  res.json({ hello: name });
});

module.exports = router;
