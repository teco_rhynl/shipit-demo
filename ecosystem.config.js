module.exports = {
  apps: [{
    name: 'shipit-demo',
    script: './bin/www',
    cwd: '/home/mrussitto/repos/shipit-demo/current',
    error_file: '/home/mrussitto/repos/shipit-demo/logs/ag.err.log',
    out_file: '/home/mrussitto/repos/shipit-demo/logs/ag.out.log',
    instance_var: 'INSTANCE_ID',
    instances: 'max',
    exec_mode: 'cluster',
    env: {
      PORT: 3000
    },
    env_development: {
      NODE_ENV: 'devel',
      PORT: 3000
    }
  }]
}
